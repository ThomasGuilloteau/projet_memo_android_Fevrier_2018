package com.example.thomas.memo;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements RecyclerView.OnItemTouchListener {

    GestureDetector gestureDetector;
    MemoAdapter memoAdapter;
    String champ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        databaseHelper.getDatabaseName();

        RecyclerView recyclerView = findViewById(R.id.liste_memo);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        MemosDAO dao = new MemosDAO();
        List<Memo> listeMemo = dao.getMemos(this);

        memoAdapter = new MemoAdapter(listeMemo);
        recyclerView.setAdapter(memoAdapter);

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent event) {
                return true;
            }
        });

        SharedPreferences sharedPosition = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        String save = sharedPosition.getString("position", null);

        Toast.makeText(MainActivity.this, "Lors de la dernière connexion vous etiez à la position : " + save, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (gestureDetector.onTouchEvent(motionEvent)){
            View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
            if(child != null){
                int position = recyclerView.getChildAdapterPosition(child);

                AsyncHttpClient client = new AsyncHttpClient();


                RequestParams requestParams = new RequestParams();
                requestParams.put("position", position);

                RequestHandle requestHandle = client.get("http://httpbin.org/get", requestParams, new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                        String retour = new String(response);
                        try {
                            JSONObject jsonObject = new JSONObject(retour);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("args");
                            String jsonRep = jsonObject1.getString("position");
                            Toast.makeText(MainActivity.this, "Vous ête à la postion : " + jsonRep, Toast.LENGTH_LONG).show();

                            SharedPreferences sharedPosition = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                            SharedPreferences.Editor editor = sharedPosition.edit();
                            editor.putString("position", jsonRep);
                            editor.apply();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                        Log.e("Ca Marche Pas !!!", e.toString());
                    }
                });
                return true;
            }

        }

        return false;
    }


    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public void clicAjout(View view){


        MemosDAO dao = new MemosDAO();
        EditText editText  = findViewById(R.id.recherche);
        champ = String.valueOf(editText.getText());
        dao.ajouter(this, champ);
        memoAdapter.update(dao.getMemos(this));
        editText.setText("");

    }
}
