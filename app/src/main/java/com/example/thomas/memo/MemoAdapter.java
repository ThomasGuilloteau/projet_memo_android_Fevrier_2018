package com.example.thomas.memo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by thomas on 15/02/2018.
 */

public class MemoAdapter extends RecyclerView.Adapter<MemoViewHolder> {

    private List<Memo> listMemo = null;

    public MemoAdapter(List<Memo> listMemo) {
        this.listMemo = listMemo;
    }

    @Override
    public MemoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewMemo = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_memo, parent, false);
        return new MemoViewHolder(viewMemo);
    }

    @Override
    public void onBindViewHolder(MemoViewHolder holder, int position) {
        holder.getTextViewLibelleMemo().setText(listMemo.get(position).getIntitule());

    }

    @Override
    public int getItemCount() {
        return listMemo.size();
    }

    public void update(List<Memo> listMemo) {
        this.listMemo = listMemo;
        notifyDataSetChanged();
    }
}
