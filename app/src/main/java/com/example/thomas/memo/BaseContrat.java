package com.example.thomas.memo;

import android.provider.BaseColumns;

/**
 * Created by thomas on 15/02/2018.
 */

public class BaseContrat {

    private BaseContrat(){

    }

    public static class MemoContrat implements BaseColumns{

        public static final String TABLE_MEMOS = "memos";
        public static final String COLONNE_INTITULE = "intitule";
    }
}
