package com.example.thomas.memo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.Date;

/**
 * Created by thomas on 14/02/2018.
 */

public class MemoViewHolder extends RecyclerView.ViewHolder{


    private TextView textViewLibelleMemo = null;

    public MemoViewHolder(View itemView){

        super(itemView);
        textViewLibelleMemo = itemView.findViewById(R.id.libelle_memo);
    }



    public TextView getTextViewLibelleMemo(){

        return textViewLibelleMemo;
    }

}
