package com.example.thomas.memo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by thomas on 15/02/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String NOM_BASE = "Memo.db";
    private static final int VERSION = 1;

    public DatabaseHelper(Context context){
        super(context, NOM_BASE, null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Memos(_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , intitule TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
}
