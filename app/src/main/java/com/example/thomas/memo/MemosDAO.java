package com.example.thomas.memo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomas on 15/02/2018.
 */

public class MemosDAO {
    public List<Memo> getMemos(Context context) {

        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String[] projection = {BaseContrat.MemoContrat._ID,
                BaseContrat.MemoContrat.COLONNE_INTITULE};


        String tri = BaseContrat.MemoContrat.COLONNE_INTITULE + " DESC ";

        Cursor cursor = db.query(
                BaseContrat.MemoContrat.TABLE_MEMOS,
                projection,
                null,
                null,
                null,
                null,
                tri,
                null);


        List<Memo> listeMemo = new ArrayList<>();

        if (cursor != null) {

            try {

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    listeMemo.add(new Memo(cursor.getString(cursor.getColumnIndex(BaseContrat.MemoContrat.COLONNE_INTITULE))));
                    cursor.moveToNext();
                }
            } catch (Exception exception) {

                exception.printStackTrace();
            } finally {
                cursor.close();
            }
        }
        return listeMemo;

    }

    public void ajouter(Context context, String libelle) {

        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BaseContrat.MemoContrat.COLONNE_INTITULE, libelle);

        long id = db.insert(BaseContrat.MemoContrat.TABLE_MEMOS, null, values);
    }

}
